part of gomma_rotation;
class Details{
  html.Element node=html.document.query("#details");
  List detail;
stagexl.Transition t;
var isShown=false;
  Details(){
    node.onClick.listen(_handleClickChiudi);
    final width=-DomUtil.clientSize(node).width;
    node.style.transform=CssUtil.translate3d(width.toInt(), 0,0);

  }
  _handleClickChiudi(e){
    e.preventDefault();
    hide();
  }
  update(detail){
    this.detail=detail;
  }
  show(index){
    isShown=true;
   // node.style.display="block";
    final width=-DomUtil.clientSize(node).width;
    node.style.transform=CssUtil.translate3d(width.toInt(), 0,0);
    node.query(".title").text=detail[index]["title"];
    node.query(".description").text=detail[index]["description"];
   _animateShow();

  }
  _animateShow(){
    if(t!=null){
      Assets.renderLoop.juggler.remove(t);
    }
    Assets.renderLoop.removeStage(Assets.stage);
    final point=CssUtil.point3DOf(node.style.transform);
    t= Assets.renderLoop.juggler.transition(point.x, 0, .5, stagexl.TransitionFunction.easeOutSine, (value){
       node.style.transform=CssUtil.translate3d(value.toInt(), 0,0);
       });
  }
  hide(){
    isShown=false;
    Assets.renderLoop.addStage(Assets.stage);
    if(t!=null){
      Assets.renderLoop.juggler.remove(t);
    }
    final _width=-DomUtil.clientSize(node).width;
    final point=CssUtil.point3DOf(node.style.transform);
   t= Assets.renderLoop.juggler.transition(point.x, _width, .5, stagexl.TransitionFunction.easeInOutSine, (value){
     node.style.transform=CssUtil.translate3d(value.toInt(), 0,0);

    });
  }
}

