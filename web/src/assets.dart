part of gomma_rotation;
class Assets{
   static final  stagexl.RenderLoop renderLoop=new stagexl.RenderLoop();
   static final stagexl.ResourceManager resourceManager = new stagexl.ResourceManager();
   static var bigTyres;
   static  stagexl.Stage stage;
   static init(){
    // stagexl.BitmapData.defaultLoadOptions = new stagexl.BitmapDataLoadOptions(png:true, jpg:true, webp:true);
     //stagexl.BitmapData.defaultLoadOptions.webp = true;
    // stagexl.BitmapData.defaultLoadOptions.png = true;
   }
   static Future<stagexl.ResourceManager> firstLoad(){
     resourceManager.addBitmapData("Point", "imgs/point.png");

     resourceManager.addBitmapData("0", bigTyres[0]);
     return resourceManager.load();
   }
   static Future<stagexl.ResourceManager> load(){
     for(var i=1;i<bigTyres.length;i++){
       resourceManager.addBitmapData(i.toString(), bigTyres[i]);
     }
     return resourceManager.load();
   }

   static var start_time = new DateTime.now().millisecondsSinceEpoch ;
   static int getTimer () {
     return new DateTime.now().millisecondsSinceEpoch- start_time; //milliseconds
   }
}