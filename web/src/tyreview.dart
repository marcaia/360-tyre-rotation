part of gomma_rotation;

class TyreView extends stagexl.Sprite{
  int currentFrame=0;
  stagexl.Bitmap image;
  StreamSubscription down,up,move;
  html.Point diff;
  var transition=0,OFFSET=0;
  final html.Point _POSITION=new html.Point(0, 0);
  final html.Point _OFFSET_POSITION=new html.Point(0, 0);
  html.Point center(stagexl.DisplayObject node){
    _POSITION.x=node.x;
    _POSITION.y=node.y;
    _OFFSET_POSITION.x=node.width / 2;
    _OFFSET_POSITION.y= node.height / 2;
    return _POSITION + _OFFSET_POSITION;
  }

  Transformation trans= new Transformation.identity();

  final StreamController _onFrameChangedController = new StreamController();
  Stream get onFrameChanged => _onFrameChangedController.stream;
  //
  final StreamController _onTransformChangedController = new StreamController();
  Stream get onTransformChanged => _onTransformChangedController.stream;
  PointsView pointsView;
  BigThumb bigThumb;
  var initX=0,initY=0;
  stagexl.Bitmap hitSprite;
  void init(pointsView){
    hitSprite=new stagexl.Bitmap(new stagexl.BitmapData(640, 640, false, stagexl.Color.Aquamarine));
    hitSprite.alpha=0.3;
    addChild(hitSprite);
    this.pointsView=pointsView;
    image=new stagexl.Bitmap(Assets.resourceManager.getBitmapData(currentFrame.toString()));
    addChild(image);
    //
    hitSprite.pivotX=hitSprite.width/2;
    hitSprite.pivotY=hitSprite.height/2;
    hitSprite.x=hitSprite.width/2;
    hitSprite.y=hitSprite.height/2;
    initX=hitSprite.x;
    initY=hitSprite.y;
    //
    /*image.pivotX=image.width/2;
    image.pivotY=image.height/2;
    image.x=image.width/2;
    image.y=image.height/2;
    initX=image.x;
    initY=image.y;*/
  }
  void complete(){
    DragGesture dragG= new DragGesture(html.document.query("#stage"), move: (DragGestureState state) {
      final _to=OFFSET+state.transition.x/12;
      dragMove(_to);
    }, end: (DragGestureState state) {
      dragEnd();
    },start:(DragGestureState state){
      dragStart();
    });


   ZoomGesture zoomG= new ZoomGesture(html.document.query("#stage"), start: (ZoomGestureState state) {
      zoomStart(center(hitSprite) - state.startMidpoint);
    }, move: (ZoomGestureState state) {
      zoomMove(state);
    }, end: (ZoomGestureState state) {
      zoomEnd(state.transformation.originAt(diff) * trans);
    });

  }
  dragStart(){
    OFFSET=transition;
  }
  dragMove(_to){
    Assets.renderLoop.juggler.transition(transition,_to, 1, stagexl.TransitionFunction.easeOutCircular, (value){
      gotoAndStop(value.round());
    });
    pointsView.mouseEnabled=false;
    pointsView.mouseChildren=false;
    pointsView.alpha=0.3;
  }
  dragEnd(){
    pointsView.mouseEnabled=true;
    pointsView.mouseChildren=true;
    pointsView.alpha=1;
  }
  zoomStart(_diff){
    diff = _diff;
    initX=hitSprite.x;
    initY=hitSprite.y;
    //
    pointsView.mouseEnabled=false;
    pointsView.mouseChildren=false;
    pointsView.alpha=0.3;
  }
  zoomMove(state){
    final t = state.transformation.originAt(diff)* trans;
    final offsetX=(state.midpoint.x-state.startMidpoint.x);
    final offsetY=(state.midpoint.y-state.startMidpoint.y);
    //
    final stagexl.Matrix m =new stagexl.Matrix(t[0][0], t[1][0], t[0][1], t[1][1], t[0][2], t[1][2]);
    final _scale =((hitSprite.scaleX<=1)&& m.a<1)?1:m.a;
    final _x=initX+offsetX*2.5;
    final _y=initY+offsetY*2.5;
    //
    _updateXYScale(pointsView,_x,_y,_scale);
    _updateXYScale(hitSprite,_x,_y,_scale);
    _updateXYScale(image,_x,_y,_scale);
    //
    pointsView.setPointsScale(_scale);
  }
  _updateXYScale(display,x,y){
    display.x=x;
    display.y=y;
    display.scaleX=display.scaleY=_scale;
  }

  zoomEnd(_trans){
    initX=hitSprite.x;
    initY=hitSprite.y;
    trans = _trans;
    //
    pointsView.mouseEnabled=true;
    pointsView.mouseChildren=true;
    pointsView.alpha=1;
  }
  reset(){
    stagexl.Matrix m=new stagexl.Matrix.fromIdentity();
    var _scaleX=m.a;
    var _scaleY=m.d;
    var _pivotX=hitSprite.pivotX;
    var _pivotY=hitSprite.pivotY;
    hitSprite.setTransform(hitSprite.pivotX,hitSprite.pivotY,_scaleX,_scaleY,0,0,0,_pivotX,_pivotY);
    image.setTransform(hitSprite.pivotX,hitSprite.pivotY,_scaleX,_scaleY,0,0,0,_pivotX,_pivotY);
    pointsView.setTransform(hitSprite.pivotX,hitSprite.pivotY,_scaleX,_scaleY,0,0,0,_pivotX,_pivotY);
    pointsView.setPointsScale(_scaleX);
    trans = new Transformation.identity();
  }
  gotoAndStop(value){
    transition=value;
    int newValue=value%36;
    if(newValue!=currentFrame){
      currentFrame=newValue;
      image.bitmapData=Assets.resourceManager.getBitmapData(currentFrame.toString());

      _onFrameChangedController.add(currentFrame.toString());
    }
  }

}
