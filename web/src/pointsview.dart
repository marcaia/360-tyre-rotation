part of gomma_rotation;
class PointsView extends stagexl.Sprite{
  List detail;
  final StreamController _onClickController = new StreamController();
  Stream get onClickPoint => _onClickController.stream;
  var frame;
  List _points;
  var _scale=1;
PointsView(){
  pivotX=1024/2;
  pivotY=767/2;
  x=pivotX;
  y=pivotY;
}

    update(_frame,_detail){
      this.detail=_detail;
      this.frame=_frame;
      visible=true;
    _createPoints();
    }
    hide(){
     visible=false;
    }
    _createPoints(){
      if(numChildren>0)removeChildren();

      _points=new List();
      for(var i=0;i<detail.length;i++){

        var point=new PointComponent();
        addChild(point);
        point.x=detail[i]["x"];
        point.y=detail[i]["y"];
        point.scaleX=1/_scale;
        point.scaleY=1/_scale;
        point.onMouseClick.listen((e)=>_onClickController.add(i));
        point.onTouchEnd.listen((e)=>_onClickController.add(i));
        point.init();
        _points.add(point);
      }
     // _posiziona();
    }
    setPointsScale(_scale){
      this._scale=_scale;
      if(_points!=null){
        for(var i=0;i<_points.length;i++){
          _points[i].scaleX=1/_scale;
          _points[i].scaleY=1/_scale;
        }
      }
      //_posiziona();

    }
}
class PointComponent extends stagexl.Sprite{

  init(){
    var image=new stagexl.Bitmap(Assets.resourceManager.getBitmapData("Point"));
    addChild(image);
    image.x=-image.width/2;
    image.y=-image.height/2;
  }
}

