part of gomma_rotation;
class ApplicationModel{
  var data;
  final StreamController _onActionController = new StreamController();
  Stream get onLoaded => _onActionController.stream;
  List images;
  List _points;
  load(url){
    final  request = html.HttpRequest.getString(url).then(_onDataLoaded);
  }
  void _onDataLoaded(response){
    data=JSON.parse(response);

   images=data["product"].toList();
   _points =data["points"].toList();
    _onActionController.add(data);
  }
  getDetail(key){
    if(data["details"][key]==null){
      return null;
    }else{
      List _details=data["details"][key].toList();

      List _result=new List();
      for (var i=0;i<_details.length;i++){
        _result.add(_points[_details[i]]) ;
      }

      return _result;
    }
    return null;

  }

}