library gomma_rotation;
import 'dart:html'as html;
import "dart:async";
import "dart:math" as Math;
import "dart:json"as JSON;
import 'package:stagexl/stagexl.dart' as stagexl;
import 'package:rikulo_ui/gesture.dart';
import 'package:rikulo_ui/event.dart';
import 'package:rikulo_commons/html.dart';

part "assets.dart";
part "tyreview.dart";
part "model/model.dart";
part "details.dart";
part "pointsview.dart";
part "bigthumb.dart";
class Application{
  stagexl.Stage stage=new stagexl.Stage("mystage",html.document.query("#stage"));

  TyreView tyreview=new TyreView();
  Details details=new Details();
  PointsView pointsView=new PointsView();
  final ApplicationModel model=new ApplicationModel();
  Application(){
    _modelPrep();
  }
  _modelPrep(){
    model.load("json/assets.json");
    model.onLoaded.listen(_viewPrep);
  }
  _viewPrep(result){
    if (stagexl.Multitouch.supportsTouchEvents) {
      stagexl.Multitouch.inputMode = stagexl.MultitouchInputMode.TOUCH_POINT;
    }
    Assets.stage=stage;
    Assets.bigTyres=model.images;
    Assets.renderLoop.addStage(stage);
    Assets.init();
    Assets.firstLoad().then(startAnimation);
    Assets.load().then(_handleAssetsLoadCompleted);

  }
  startAnimation(result){
    stage.scaleMode=stagexl.StageScaleMode.SHOW_ALL;

    stage.addChild(tyreview);
    stage.addChild(pointsView);
    tyreview.init(pointsView);
    pointsView.onClickPoint.listen(_handleClickPoint);
    html.document.query("#reset").onClick.listen((e)=>tyreview.reset());

  }
  _handleAssetsLoadCompleted(result){
    tyreview.complete();
    html.document.query("#preloader").remove();
    tyreview.onFrameChanged.listen(_handleFrameChanged);


  }
 _handleFrameChanged(_frame){
   var detail=model.getDetail(_frame);

   if(detail!=null){
     details.update(detail);
     pointsView.update(_frame, detail);
   } else{
     pointsView.hide();
   }
  }
  _handleClickPoint(index){
    details.show(index);
  }
}
void main(){
  var application=new Application();

}
